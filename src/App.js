import React from "react";
import "./App.css";
import Location from "./container/Location";
import { BrowserRouter as Router, Route } from "react-router-dom";
function App() {
  return (
    <Router>
      <Route path="/" component={Location} />
    </Router>
  );
}

export default App;
