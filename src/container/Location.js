import React, { Component } from "react";
import ListView from "./listView";
import AddLocation from "./addLocation";
import AddTimes from "./addTimes";
import { addLocationData, addTimesData, locations } from "../constant";
import { LocationContext } from "../context";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Location extends Component {
  constructor(props) {
    super(props);
    // handle onchange of location form
    this.onLocationChange = (e) => {
      const { locationData } = this.state;
      let data = locationData;
      if (e.target.value) {
        data[e.target.id].error = false;
      }
      if (e.target.id === "contact") {
        // US mobile validation
        const isValidMobile = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/.test(
          e.target.value
        );
        const isValidString = /^[^a-zA-Z!@#$%^&*_+\=\[\]{};':"\\|,.<>\/?]*$/.test(
          e.target.value
        );
        if (data[e.target.id].value !== "" && !isValidMobile && isValidString) {
          data[e.target.id].error = true;
        }
        if (isValidString) {
          data[e.target.id].value = e.target.value;
        }
      } else if (e.target.id === "code") {
        // code must be greater than 5 and less than 10
        const isSpace = /\s/.test(e.target.value);
        if (e.target.value.length <= 10) {
          if (!isSpace) {
            data[e.target.id].value = e.target.value;
          }
          if (data[e.target.id].value.length < 5) {
            data[e.target.id].error = true;
          }
          if (data[e.target.id].value === "") {
            data[e.target.id].error = false;
          }
        }
      } else if (e.target.id === "appoinmentPool") {
        // create tags form input value
        let value = e.target.value.split(",");
        let tags = data[e.target.id].tags;
        value.length > 1 && value[0] !== "" && tags.push(value[0]);
        data[e.target.id].tags = tags;
        data[e.target.id].value = value[value.length - 1];
      } else {
        data[e.target.id].value = e.target.value;
      }
      this.setState({ locationData: data });
    };
    // handle checkbox
    this.onCheckboxClick = (index) => {
      const { timesData } = this.state;
      let data = timesData;
      data[index].checked = !data[index].checked;
      this.setState({ timesData: data });
    };
    // handle "Apply to all checked" button
    this.selectAll = () => {
      const { timesData } = this.state;
      let data = timesData;
      for (let i = 0; i < data.length; i++) {
        data[i].checked = true;
      }
      this.setState({ timesData: data });
    };
    // handle times onchange
    this.onChange = (index, key, value) => {
      const { timesData } = this.state;
      let data = timesData;
      data[index][key].value = value;
      this.setState({ timesData: data });
    };
    // handle timezone change
    this.onChangeZone = (index, key, zone) => {
      const { timesData } = this.state;
      let data = timesData;
      data[index][key].zone = zone;
      this.setState({ timesData: data });
    };
    // input time validation
    this.onBlur = (index, key, value) => {
      const { timesData } = this.state;
      var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(
        value
      );
      if (!isValid) {
        let data = timesData;
        data[index][key].value = key === "from" ? "10:30" : "6:30";
        this.setState({ timesData: data });
      } else {
        let check = timesData[index][key].value.split(":");
        if (parseInt(check[0]) >= 12) {
          let data = timesData;
          data[index][key].value = `${parseInt(check[0]) - 12}:${check[1]}`;
          this.setState({ timesData: data });
        }
      }
    };
    // handle cancel button of location form
    this.resetLocationForm = () => {
      this.setState({ locationData: { ...addLocationData } });
      this.props.history.push("/");
    };
    // add and edit location
    this.addLocation = (e) => {
      e.preventDefault();
      let { locations, locationData, timesData, locationId } = this.state;
      let data = locations;
      if (locationId) {
        // edit location
        data.map((location) => {
          if (location.id === locationId) {
            Object.keys(locationData).map((key) => {
              if (locationData[key].type === "tags") {
                location[locationData[key].id] = [...locationData[key].tags];
              } else {
                location[locationData[key].id] = locationData[key].value;
              }
            });
            let times = [];
            timesData.map(({ name, from, to, checked }) =>
              times.push({ name, from: { ...from }, to: { ...to }, checked })
            );
            location.times = times;
          }
        });
      } else {
        // add location
        let location = { times: [] };
        Object.keys(locationData).map((key) => {
          if (locationData[key].type === "tags") {
            location[locationData[key].id] = [...locationData[key].tags];
          } else {
            location[locationData[key].id] = locationData[key].value;
          }
        });
        let times = [];
        timesData.map(({ name, from, to, checked }) =>
          times.push({ name, from: { ...from }, to: { ...to }, checked })
        );
        location.times = times;
        location.id = locations.length + 1;
        data.push(location);
      }
      this.setState({ locations: data, locationId: "" });
      this.props.history.push("/");
    };
    // handle onclick of edit location button
    this.onEditLocation = (id) => {
      const { locations } = this.state;
      const selectedLocation = locations.filter(
        (location) => location.id === id
      );
      let locationData1 = addLocationData;
      Object.keys(locationData1).map((key) => {
        locationData1[key].value = selectedLocation[0][key];
        locationData1[key].error = false;
      });
      locationData1["appoinmentPool"].value = "";
      locationData1["appoinmentPool"].tags =
        selectedLocation[0]["appoinmentPool"];
      const timesData1 = selectedLocation[0].times;
      this.setState({
        locationData: locationData1,
        timesData: timesData1,
        locationId: id,
      });
      this.props.history.push(`/add_location?id=${id}`);
    };
    // handle delete location button
    this.onDeleteLocation = (id) => {
      const { locations } = this.state;
      const data = locations.filter((location) => location.id !== id);
      this.setState({ locations: data });
    };
    // handle submit of location form
    this.goToTimes = (e) => {
      e.preventDefault();
      const { locationData, locationId } = this.state;
      let data = locationData;
      let error = false;
      Object.keys(data).map((row) => {
        if ((data[row].required && !data[row].value) || data[row].error) {
          data[row].error = true;
          error = true;
        }
      });
      this.setState({ locationData: data });
      if (!error) {
        if (locationId) {
          this.props.history.push(`/add_times?id=${locationId}`);
        } else {
          this.props.history.push(`/add_times`);
        }
      }
    };
    // handle cancel of times form
    this.resetTimesForm = () => {
      const { locationId } = this.state;
      if (locationId) {
        this.props.history.push(`/add_location?id=${locationId}`);
      } else {
        this.props.history.push(`add_location`);
      }
    };
    this.state = {
      onLocationChange: this.onLocationChange,
      onCheckboxClick: this.onCheckboxClick,
      selectAll: this.selectAll,
      onChange: this.onChange,
      onChangeZone: this.onChangeZone,
      onBlur: this.onBlur,
      resetLocationForm: this.resetLocationForm,
      addLocation: this.addLocation,
      resetTimesForm: this.resetTimesForm,
      goToTimes: this.goToTimes,
      onDeleteLocation: this.onDeleteLocation,
      onEditLocation: this.onEditLocation,
      locationId: "",
      locations: [],
      locationData: addLocationData,
      timesData: addTimesData,
    };
  }
  onClickAddLocation = () => {
    // reset forms
    const { locationData, timesData } = this.state;
    Object.keys(locationData).map((row) => {
      locationData[row].value = "";
      locationData[row].error = false;
    });
    locationData["appoinmentPool"].tags = [];
    timesData.map((row) => {
      row.checked = false;
      row.from.zone = "am";
      row.from.value = "10:30";
      row.to.zone = "pm";
      row.to.value = "6:30";
    });
    this.setState({ locationId: "", locationData, timesData });
    this.props.history.push("/add_location");
  };
  render() {
    return (
      <LocationContext.Provider value={this.state}>
        <div className="App">
          <div className="header">
            <div>Locations</div>
            <button onClick={this.onClickAddLocation}>+ Add Location</button>
          </div>
          <div className="container">
            <Route exact path="/" component={ListView} />
            <Route path="/add_location" component={AddLocation} />
            <Route path="/add_times" component={AddTimes} />
          </div>
        </div>
      </LocationContext.Provider>
    );
  }
}

export default Location;
