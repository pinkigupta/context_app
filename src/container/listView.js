import React, { useState } from "react";
import { LocationContext } from "../context";
import { paginationOptions } from "../constant";
import Pagination from "../components/pagination";

const NO_LOCATION = require("../assets/no_locn.png");

const ListView = () => {
  const [start, setStartIndex] = useState(0);
  const [end, setEndIndex] = useState(paginationOptions[0]);
  const onChangePage = ({ start, end }) => {
    setStartIndex(start);
    setEndIndex(end);
  };
  return (
    <LocationContext.Consumer>
      {({ locations, onEditLocation, onDeleteLocation }) => {
        if (locations.length) {
          let data = locations;
          let listItems = data.slice(start, end);
          return (
            <ul>
              <li className="heading">
                <div className="item index"></div>
                <div className="item">Location Name</div>
                <div className="item">Address</div>
                <div className="item">Phone No.</div>
                <div className="item ctn_icn"></div>
              </li>
              {listItems.map((list) => (
                <li key={list.id}>
                  <div className="item index">
                    <div className="blue_circle">{list.id}</div>
                  </div>
                  <div className="item">{list.name}</div>
                  <div className="item">{list.addressLine1}</div>
                  <div className="item">{list.contact}</div>
                  <div className="item ctn_icn">
                    <img
                      src="https://img.icons8.com/dotty/80/000000/edit.png"
                      onClick={() => onEditLocation(list.id)}
                      alt="edit"
                    />
                    <img
                      src="https://img.icons8.com/windows/32/000000/delete-forever.png"
                      onClick={() => onDeleteLocation(list.id)}
                      alt="delete"
                    />
                  </div>
                </li>
              ))}
              <li>
                <Pagination items={locations} onChangePage={onChangePage} />
              </li>
            </ul>
          );
        } else {
          return (
            <div className="no_locn">
              <img src={NO_LOCATION} alt="location" />
            </div>
          );
        }
      }}
    </LocationContext.Consumer>
  );
};

export default ListView;
