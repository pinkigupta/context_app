import React from "react";
import { LocationContext } from "../context";
import Timer from "../components/times";

const Row = ({
  data,
  onCheckboxClick,
  selectAll,
  onChange,
  onChangeZone,
  onBlur,
}) => {
  return data.map((row, i) => {
    return (
      <div className="col4" key={row.name}>
        <div className="col2-2 ctn_chk">
          <input
            type="checkbox"
            onChange={() => onCheckboxClick(i)}
            checked={row.checked}
          />
          <div className="label">{row.name}</div>
        </div>
        <Timer
          onChange={(e) => onChange(i, "from", e.target.value)}
          values={row.from}
          onChangeZone={(val) => onChangeZone(i, "from", val)}
          onBlur={(e) => onBlur(i, "from", e.target.value)}
        />
        <Timer
          onChange={(e) => onChange(i, "to", e.target.value)}
          values={row.to}
          onChangeZone={(val) => onChangeZone(i, "to", val)}
          onBlur={(e) => onBlur(i, "to", e.target.value)}
        />
        <div className="btn">
          <button type="button" onClick={selectAll}>
            Apply to All Checked
          </button>
        </div>
      </div>
    );
  });
};

const AddTimes = () => {
  return (
    <LocationContext.Consumer>
      {({
        timesData,
        onCheckboxClick,
        selectAll,
        onChange,
        onChangeZone,
        onBlur,
        addLocation,
        resetTimesForm,
      }) => (
        <form className="add_times" onSubmit={addLocation}>
          <h1 className="title">Faculty Times</h1>
          <div className="col4 head">
            <div className="label" />
            <div className="label">From</div>
            <div className="label">To</div>
            <div />
          </div>
          <Row
            data={timesData}
            onCheckboxClick={onCheckboxClick}
            selectAll={selectAll}
            onChangeZone={onChangeZone}
            onChange={onChange}
            onBlur={onBlur}
          />
          <div className="bottom">
            <button type="button" className="cancel" onClick={resetTimesForm}>
              Cancel
            </button>
            <button type="submit">Save</button>
          </div>
        </form>
      )}
    </LocationContext.Consumer>
  );
};

export default AddTimes;
