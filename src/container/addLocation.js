import React from "react";
import { LocationContext } from "../context";
import { Link } from "react-router-dom";
import Input from "../components/input";

const AddLocation = () => {
  return (
    <LocationContext.Consumer>
      {({ locationData, onLocationChange, resetLocationForm, goToTimes }) => (
        <form onSubmit={goToTimes}>
          <h1 className="title">Add Location</h1>
          <Input {...locationData.name} onLocationChange={onLocationChange} />
          <div className="col2">
            <Input
              {...locationData.addressLine1}
              onLocationChange={onLocationChange}
            />
            <Input
              {...locationData.suiteNo}
              onLocationChange={onLocationChange}
            />
          </div>
          <div className="col2">
            <Input
              {...locationData.addressLine2}
              onLocationChange={onLocationChange}
            />
            <div className="col2-2">
              <Input
                {...locationData.city}
                onLocationChange={onLocationChange}
              />
              <Input
                {...locationData.state}
                onLocationChange={onLocationChange}
              />
            </div>
          </div>
          <div className="col2">
            <div className="col2-2">
              <Input
                {...locationData.code}
                onLocationChange={onLocationChange}
              />
              <Input
                {...locationData.contact}
                onLocationChange={onLocationChange}
              />
            </div>
            <Input
              {...locationData.timeZone}
              onLocationChange={onLocationChange}
            />
          </div>
          <div className="col2">
            <div>
              <div className="label">{locationData.facultyTimes.label}</div>
              <Link to="/add_times">
                <div className="clk" />
              </Link>
            </div>
            <Input
              {...locationData.appoinmentPool}
              onLocationChange={onLocationChange}
            />
          </div>
          <div className="bottom">
            <button
              className="cancel"
              type="button"
              onClick={resetLocationForm}
            >
              Cancel
            </button>
            <button type="submit">Save</button>
          </div>
        </form>
      )}
    </LocationContext.Consumer>
  );
};

export default AddLocation;
