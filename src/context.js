import React from 'react';
import { addLocationData, addTimesData } from "./constant";

export const LocationContext = React.createContext({
    locations: [],
    locationData: addLocationData,
    timesData: addTimesData,
    isTimesFormOpen: false,
    isLocationFormOpen: false,
    locationId: "",
    onLocationChange: (e) => {},
    handleTimesForm: (e) => {},
    resetLocationForm: (e) => {},
    addLocation: (e) => {},
    onEditLocation: (e) => {},
    onDeleteLocation: (e) => {},
    redirectToAddTimes: (e) => {},
    goToTimes: () => {}
  });
  