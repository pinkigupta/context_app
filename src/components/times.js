import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  values: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onChangeZone: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
};

const defaultProps = {
  values: {
    value: "00:00",
    zone: "am",
  },
  onChange: () => {},
  onChangeZone: () => {},
  onBlur: () => {},
};

const Timer = ({ values, onChange, onChangeZone, onBlur }) => {
  const { value, zone } = values;
  return (
    <div className="col2-2 ctn_inp">
      <input onChange={onChange} value={value} onBlur={onBlur} />
      <div className="time_zone">
        <span
          className={`am ${zone === "am" ? "selected" : ""}`}
          onClick={() => onChangeZone("am")}
        >
          AM
        </span>
        <span
          className={`pm ${zone === "pm" ? "selected" : ""}`}
          onClick={() => onChangeZone("pm")}
        >
          PM
        </span>
      </div>
    </div>
  );
};

Timer.propsTypes = propTypes;
Timer.defaultProps = defaultProps;
export default Timer;
