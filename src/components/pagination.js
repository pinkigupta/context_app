import React, { useState } from "react";
import { paginationOptions } from "../constant";
import PropTypes from "prop-types";

const propTypes = {
  items: PropTypes.array.isRequired,
  onChangePage: PropTypes.func.isRequired,
  options: PropTypes.array,
  initialPage: PropTypes.number,
  pageSize: PropTypes.number,
};

const defaultProps = {
  initialPage: 1,
  options: paginationOptions,
  pageSize: 10,
};

const Pagination = ({
  initialPage = 1,
  items = [],
  onChangePage,
  options,
  size = options[0],
}) => {
  const [pageSize, setPageSize] = useState(size);
  const [currentPage, setPage] = useState(initialPage);
  const initialSize = () => {
    let totalSize = parseInt(items.length / size);
    if (items.length % size) {
      totalSize += 1;
    }
    return totalSize;
  };
  const [totalPages, setTotalPages] = useState(initialSize());
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(pageSize);
  if (end > items.length) {
    setEnd(items.length);
  }
  const goToNextPage = () => {
    if (currentPage < totalPages) {
      let end = (currentPage + 1) * pageSize;
      if (end > items.length) {
        end = items.length;
      }
      let start = currentPage * pageSize;
      setStart(start);
      setEnd(end);
      setPage(currentPage + 1);
      onChangePage({ start, end });
    }
  };
  const goToPreviousPage = () => {
    if (currentPage > 1) {
      let end = (currentPage - 1) * pageSize;
      if (end > items.length) {
        end = items.length;
      }
      let start = (currentPage - 2) * pageSize;
      setStart(start);
      setEnd(end);
      setPage(currentPage - 1);
      onChangePage({ start, end });
    }
  };
  const goToFirstPage = (size) => {
    let currentSize = pageSize;
    if (!isNaN(size)) {
      currentSize = size;
    }
    let end = currentSize;
    if (end > items.length) {
      end = items.length;
    }
    setStart(0);
    setEnd(end);
    setPage(1);
    onChangePage({ start: 0, end });
  };
  const goToLastPage = () => {
    let end = totalPages * pageSize;
    if (end > items.length) {
      end = items.length;
    }
    let start = (totalPages - 1) * pageSize;
    setStart(start);
    setEnd(end);
    setPage(totalPages);
    onChangePage({ start, end });
  };
  const onChangePageSize = (e) => {
    let size = e.target.value;
    setPageSize(size);
    goToFirstPage(size);
    let totalSize = parseInt(items.length / size);
    if (items.length % size) {
      totalSize = totalSize + 1;
    }
    setTotalPages(totalSize);
  };
  return (
    <div className="pagination">
      <div className="text">
        <span>Items per page</span>
        <select id="size" value={options[0]} onChange={onChangePageSize}>
          {options.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
      </div>
      <img
        className={start === 0 ? "disabled" : ""}
        onClick={goToFirstPage}
        src="https://img.icons8.com/material/24/000000/double-left--v1.png"
        alt="first"
      />
      <img
        className={start === 0 ? "disabled" : ""}
        onClick={goToPreviousPage}
        src="https://img.icons8.com/material/24/000000/chevron-left--v1.png"
        alt="previous"
      />
      <img
        className={totalPages === currentPage ? "disabled" : ""}
        onClick={goToNextPage}
        src="https://img.icons8.com/material-sharp/24/000000/chevron-right.png"
        alt="next"
      />
      <img
        className={totalPages === currentPage ? "disabled" : ""}
        onClick={goToLastPage}
        src="https://img.icons8.com/material-outlined/24/000000/double-right.png"
        alt="last"
      />
    </div>
  );
};

Pagination.propsTypes = propTypes;
Pagination.defaultProps = defaultProps;
export default Pagination;
