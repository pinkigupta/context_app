import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onLocationChange: PropTypes.func.isRequired,
};

const defaultProps = {
  type: "text",
};

const Input = ({
  label,
  error,
  id,
  value,
  tags,
  type,
  options,
  onLocationChange,
}) => {
  let name = <div className="label">{label}</div>;
  if (type === "text") {
    return (
      <div key={id}>
        {name}
        <input
          className={error ? "error" : ""}
          id={id}
          value={value}
          onChange={onLocationChange}
        />
      </div>
    );
  } else if (type === "select") {
    return (
      <div key={id}>
        {name}
        <select id={id} onChange={onLocationChange} value={value}>
          {options.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
      </div>
    );
  } else if (type === "tags") {
    return (
      <div key={id}>
        {name}
        <div className="tags">
          <div className="ctn_tags">
            {tags.map((tag, i) => (
              <div key={`${tag}${i}`}>{tag}</div>
            ))}
          </div>
          <input id={id} value={value} onChange={onLocationChange} />
        </div>
      </div>
    );
  }
};

Input.propsTypes = propTypes;
Input.defaultProps = defaultProps;
export default Input;
